{-# LANGUAGE FlexibleContexts #-}

import Prelude as P
import Control.Arrow (first)
import Control.Monad
import Data.Array.Repa as R
import Data.Array.Repa.IO.DevIL
import Data.Array.Repa.Repr.ForeignPtr as F
import Data.List (maximumBy)
import Data.Ord (comparing)
import qualified Data.Vector.Storable as V
import Data.Word8 (Word8)
import System.Directory (doesFileExist, removeFile)

import Morphology

main :: IO ()
main = do
    safeRemoveFile "grey.jpg" 
    safeRemoveFile "bw.jpg"
    safeRemoveFile "erosion.jpg"
    runIL $ do
        img <- readImage "test.jpg"
        grey <- greyscale img
        bw <- blackWhite grey
        open <- erosion (Box 5 5) bw
        writeImage "grey.jpg" grey
        writeImage "bw.jpg" bw
        writeImage "erosion.jpg" open

greyscale :: Image -> IL Image
greyscale (RGB rgb) = Grey `liftM` computeP (luminances rgb)
greyscale grey@(Grey _) = return grey

luminances :: (Source s Word8) => Array s DIM3 Word8 -> Array D DIM2 Word8
luminances a =
    traverse a
             (\(e :. _) -> e)
             (\f e -> luminance (f $ e :. 0) (f $ e :. 1) (f $ e :. 2))

{-# INLINE luminance #-}
luminance :: Word8 -> Word8 -> Word8 -> Word8
luminance r g b = ceiling $ 0.2126 * r' + 0.7152 * g' + 0.0722 * b'
    where r' = fromIntegral r :: Float
          g' = fromIntegral g
          b' = fromIntegral b

blackWhite :: Image -> IL Image
blackWhite img@(RGB _) = blackWhite =<< greyscale img
blackWhite (Grey grey) =
    let hist = count grey
        partialSums = scanl1 (+) $ P.map (uncurry (*) . first fromIntegral) hist
        sumAll = last partialSums
        partialCnts = scanl1 (+) $ P.map snd hist
        cntAll = last partialCnts
        variance :: Int -> Int -> Double
        variance c s =
            let cntRest = fromIntegral $ cntAll - c
                sumRest = fromIntegral $ sumAll - s
                mean = fromIntegral s / fromIntegral c
                meanRest = sumRest / cntRest
            in  fromIntegral c * cntRest * ((mean - meanRest) ** 2)
        variances = P.zipWith variance (init partialCnts) (init partialSums)
        bestT = fst . maximumBy (comparing snd) $ zip (P.map fst hist) variances
    in  Grey `liftM` computeP (R.map (binary bestT) grey)

{-# INLINE binary #-}
binary :: Word8 -> Word8 -> Word8
binary t x | x <= t    = 0
           | otherwise = maxBound

count :: Shape sh => Array F sh Word8 -> [(Word8, Int)]
count a =
    let n = R.size $ extent a
        v = V.unsafeFromForeignPtr0 (F.toForeignPtr a) n
        zeroes = V.replicate n 0
        ones = V.replicate n 1
        cnts = V.accumulate_ (+) zeroes (V.map fromIntegral v) ones
    in  filter ((/=0) . snd) $ zip [0..] $ V.toList cnts

safeRemoveFile :: FilePath -> IO ()
safeRemoveFile file = do
    exist <- doesFileExist file
    when exist $ removeFile file
