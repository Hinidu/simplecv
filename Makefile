all: SimpleCV

GHC_FLAGS = -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 -fllvm -optlo-O3 -with-rtsopts="$(RTS_OPTS)"
PROFILER_FLAGS = -prof -auto-all -caf-all
RTS_OPTS = -K64M -N2

SimpleCV: *.hs
	ghc --make $(GHC_FLAGS) SimpleCV.hs

debug: *.hs
	ghc --make $(GHC_FLAGS) $(PROFILER_FLAGS) SimpleCV.hs

clear:
	rm *.hi *.o
	rm SimpleCV
