module Morphology where

import Control.Monad
import Data.Array.Repa as R
import Data.Array.Repa.IO.DevIL

data StructuringElement = Box Int Int

indices :: StructuringElement -> [(Int, Int)]
indices (Box h w)
    | h < 0 || w < 0 = error "Box can't have negative dimensions"
    | otherwise      = let mh = h `div` 2
                           mw = w `div` 2
                       in [(i - mh, j - mw) | i <- [0..h-1], j <- [0..w-1]]

erosion :: StructuringElement -> Image -> IL Image
erosion se (Grey img) =
    let (Z :. h :. w) = extent img
        inds = indices se
        overlay f (Z :. i :. j) =
            if and [i' < 0 || i' >= h || j' < 0 || j' >= w
                           || f (Z :. i' :. j') == minBound
                   | (p, q) <- inds
                   , let i' = i + p
                   , let j' = j + q]
                then minBound
                else maxBound
    in  Grey `liftM` computeP (traverse img id overlay)
